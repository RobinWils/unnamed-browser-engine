# unnamed-browser-engine
## Short description
This browser engine is a yet unnamed project which focuses on customizability and extensibility.

It should become a browser engine with editable options. These options should make it possible to create different configurations. Some configurations could focus on privacy while others could focus on a browser with many features for average users.

### Longer description
https://robinwils.gitlab.io/articles/sbcl-browser-engine.html
 
## Programming language
[Steel Bank Common Lisp (SBCL)](http://www.sbcl.org/index.html)  
SBCL compiles to machine code and this is important for a browser engine.

## TODO's
- [ ] [Learn Common Lisp](https://gitlab.com/RobinWils/practical-common-lisp-exercises)
- [ ] Write a [toy render engine](https://limpet.net/mbrubeck/2014/08/08/toy-layout-engine-1.html)
- [ ] ...

## Notes and ideas
- It might be cool to add vi and emacs bindings
- ~~[Next Web Browser](https://github.com/atlas-engineer/next) might use it's own browser engine.  
It might be better to improve that project.  
They used WebKitGTK+ so this can't be a starting point.~~

## Project status
Project idea

## Project license
I am currently thinking of the LGPL license.  
I would like to use a license of the free software foundation.